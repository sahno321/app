const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const AdminService = require("../../services/admin.service");
const ScraperService = require("../../services/scpater.service");
const UserService = require("../../services/user.service");
const SECRET_KEY = process.env.SECRET_KEY;

class AuthController {
  static login = async (ctx) => {
    const users = [
      { id: 1, username: "admin", password: "admin", role: "admin" },
      { id: 2, username: "user", password: "user", role: "user" },
      { id: 3, username: "scraper", password: "scraper", role: "scraper" },
    ];
    const { username, password } = ctx.request.body;

    const user = users.find((u) => u.username === username); // Проверка учетных данных пользователя

    if (!user) {
      ctx.status = 401; // Unauthorized
      ctx.body = { message: "Invalid username or password" };
      return;
    }

    const passwordMatch = bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      ctx.status = 401; // Unauthorized
      ctx.body = { message: "Invalid username or password" };
      return;
    }

    const token = jwt.sign({ username, role: user.role }, SECRET_KEY, {
      expiresIn: "1h",
    });
    ctx.body = { token };
  };
  static async getAdmin(ctx, next) {
    ctx.body = { message: AdminService.getAdmin() };
    await next();
  }

  static getScraper = async (ctx, next) => {
    ctx.body = { message: ScraperService.getScraper() };
    await next();
  };
  static getUser = async (ctx, next) => {
    ctx.body = { message: UserService.getUser() };
    await next();
  };
}

module.exports = AuthController;
