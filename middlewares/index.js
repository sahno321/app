require("dotenv").config();
const SECRET_KEY = process.env.SECRET_KEY;
const jwt = require("jsonwebtoken");

class Middleware {
  static authenticate = async (ctx, next) => {
    const token = ctx.headers.authorization?.replace("Bearer", ""); // Получите токен из заголовка авторизации

    if (!token) {
      ctx.status = 401; // Unauthorized
      ctx.body = { message: "Missing authorization token" };
      return;
    }

    try {
      ctx.state.user = jwt.verify(token, SECRET_KEY); // Проверка и верификация токена // Сохранение информации о пользователе в контексте запроса

      await next();
    } catch (err) {
      ctx.status = 401; // Unauthorized
      ctx.body = { message: "Invalid token" };
    }
  };
  static async isAdmin(ctx, next) {
    if (ctx.state.user.role !== "admin") {
      ctx.status = 403;
      ctx.body = { message: "Access denied" };
      return;
    }
    await next();
  }
  static async isScraper(ctx, next) {
    if (ctx.state.user.role !== "scraper" && ctx.state.user.role !== "admin") {
      ctx.status = 403;
      ctx.body = { message: "Access denied" };
      return;
    }
    await next();
  }
  static async isUser(ctx, next) {
    await next();
  }
}
module.exports = Middleware;
