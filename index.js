require("dotenv").config();
const Koa = require("koa");
const app = new Koa();
const router = require("./src/router");
const koaBody = require("koa-bodyparser");
const port = process.env.PORT;

app.use(koaBody());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(port, function () {
  console.log(`Server running on https://localhost:${port}`);
});
